---
title: Stylo
lang: fr
description: Stylo, un service tiers hébergé par Huma-Num et lien vers sa documentation.
---

# Stylo

!!! attention "À savoir"
    Les services tiers sont des outils et des plateformes dont la conception,
    la maintenance et la documentation sont assurés par  d'autres instances ou
    organisations que la TGIR Huma-Num. La TGIR Huma-Num met à disposition des
    machines virtuelles hébergées sur son infrastructure pour assurer l'accessibilité
    de ces outils et soutenir leur usage par la communauté SHS.

_Stylo_ est un éditeur de texte scientifique simplifiant la rédaction et l'édition d'articles scientifiques en Sciences Humaines et Sociales.

_Stylo_ est développé par la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca), avec le soutien d'[Érudit](http://erudit.org/) et de [la TGIR Huma-Num](https://www.huma-num.fr/), sous licence [GPL-3.0](https://github.com/EcrituresNumeriques/stylo/blob/master/LICENSE).

_Stylo_ est désormais connecté à [HumanID](https://humanid.huma-num.fr/) et accessible à tous les utilisateurs et utilisatrices ayant un compte à HumanID. [Pour en savoir plus](https://humanum.hypotheses.org/6311).

!!! note
    - Accéder à l'instance _Stylo_ hébergée par la TGIR Huma-Num : [stylo.huma-num.fr](https://stylo.huma-num.fr)
    - Voir [la documentation de _Stylo_](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)
