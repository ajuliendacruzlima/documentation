# Présentation de la TGIR - périmètre de ses services

Huma-Num propose un ensemble de services pour les données numériques produites en sciences humaines et sociales.
À chaque étape du cycle de vie des données correspond un service dédié.


## Missions de la TGIR Huma-Num

Huma-Num est une très grande infrastructure de recherche (TGIR) visant à faciliter le tournant numérique de la recherche en sciences humaines et sociales.

La mission première d’Huma-Num est d’assurer la préservation, la valorisation et l’utilisation du patrimoine scientifique des programmes de recherche en sciences humaines et sociales (SHS), et plus particulièrement des données et documents : corpus, bases de données, systèmes d’information, enquêtes, données d’observation, cartes, photographies, vidéos, enregistrements sonores.

Pour remplir cette mission, la TGIR Huma-Num est bâtie sur une organisation consistant à mettre en œuvre un dispositif humain (concertation collective au sein de Consortiums) et technologique (des services numériques pérennes) à l’échelle nationale et européenne en s’appuyant sur un important réseau de partenaires et d’opérateurs.

La TGIR Huma-Num favorise ainsi, par l’intermédiaire de consortiums regroupant des acteurs des communautés scientifiques, la coordination de la production raisonnée et collective de corpus de sources (recommandations scientifiques, bonnes pratiques technologiques).

Elle développe également un dispositif technologique unique permettant le traitement, la conservation, l’accès et l’interopérabilité des données de la recherche. Ouvert à l’ensemble des programmes de recherche de l’enseignement supérieur et de la recherche (UMR, UMS, EA, etc.), il est composé de services numériques dédiés, d’une plateforme d’accès unifié (ISIDORE) et d’une procédure d’archivage à long terme.

La TGIR Huma-Num propose en outre des guides de bonnes pratiques technologiques généralistes à destination des chercheurs. Elle peut mener ponctuellement des actions d’expertise et de formation.

Enfin, elle porte la participation de la France dans les ERIC (European Research Infrastructure Consortium) DARIAH et CLARIN en coordonnant les contributions nationales. Elle est également impliquée depuis 2015 dans deux projets H2020 : Parthenos et Humanities at Scale.

## À qui s’adressent les services d’Huma-Num ?

Les services d’Huma-Num sont accessibles aux programmes de recherche scientifiques menés collectivement par des équipes de recherche en sciences humaines et sociales. Les programmes de recherche peuvent être portés par des enseignant.e.s-chercheur.e.s, des ingénieur.e.s, des doctorant.e.s et des chercheur.e.s en post-doctorat.

Les services d’Huma-Num sont ouverts à l’ensemble de la communauté universitaire nationale (unité de recherche universitaire, équipe d’accueil, unité mixe de recherche, unité de services et de recherche, maison des sciences de l’Homme mais aussi aux Ifre, Umifre et écoles françaises à l’étranger).

## Contacter la TGIR

Pour contacter les équipes de la TGIR Huma-Num, envoyez un mail à [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr).
