# GitLab : hébergement, versionning et partage de code

L’instance GitLab d’Huma-Num permet l’hébergement sécurisé et le partage maîtrisé de fichiers, dont des fichiers de code (tous langages) selon le protocole de versionnage _git_.

Il s’agit d’une implémentation du logiciel [Gitlab](https://about.gitlab.com/).

Les principales fonctionnalités sont la gestion de version et des dépôts (_git_), l’intégration continue, la génération de sites web statiques (sous la forme d'ensemble de pages), la gestion de tickets (_issues_).

Les dépôts publics créés dans l'instance GitLab d'Huma-Num sont diffusés sur la plateforme ["Codes sources du secteur public" Code.gouv.fr](https://code.gouv.fr) développée par [Etalab](https://etalab.gouv.fr) offrant une visibilité sur les codes sources des organisme publics français. Ils sont aussi archivés dans le cadre de [Software Heritage](https://www.softwareheritage.org).

**Demander l’ouverture d’un compte Gitlab** : la demande d’un compte Gitlab se fait à partir de l’interface [HumanID](https://humanid.huma-num.fr). Pour cela, le cas échéant il est nécessaire de disposer d’un compte HumanID ([voir la documentation](humanid.md)).

Accès au service hébergé par Huma-Num : [gitlab.huma-num.fr](https://gitlab.huma-num.fr/).
