---
title: Les documentations des services tiers
lang: fr
description: Liste des services tiers hébergés par Huma-Num et liens vers leur documentation.
---

# Services tiers et documentation

!!! attention "À savoir"
    Les services tiers sont des outils et des plateformes dont la conception,
    la maintenance et la documentation sont assurés par  d'autres instances ou
    organisations que la TGIR Huma-Num. La TGIR Huma-Num met à disposition des
    machines virtuelles hébergées sur son infrastructure pour assurer l'accessibilité
    de ces outils et soutenir leur usage par la communauté SHS.

## Heurist

Heurist est un logiciel open source qui permet l’élaboration de bases de données relationnelles richement structurées. Par simple navigateur web et sans installation ni programmation, Heurist permet de créer une base de données MySQL[^1].

[^1]: Source [MATE-SHS](https://mate-shs.cnrs.fr/actions/tutomate/tuto26-heurist-ian-johnson/)

Heurist a été conçu par [Ian Johnson](http://sydney.academia.edu/Johnson), directeur fondateur de Arts eResearch (anciennement Archaeological Computing Laboratory) de l'Université de Sydney, où il a été développé de septembre 2005 à décembre 2013. Heurist continue d'être développé et soutenu au sein de la faculté des arts et des sciences sociales et du portefeuille de recherche de l'université, sous la direction de Ian Johnson.

!!! note
    - Voir l'instance _Heurist_ hébergée par la TGIR Huma-Num : [heurist.huma-num.fr](https://heurist.huma-num.fr)
    - Voir [la documentation d'Heurist](https://heuristnetwork.org/)
    - Participer à la liste des utilisateurs de _Heurist_ : [heurist-utilisateurs@groupes.renater.fr](mailto:heurist-utilisateurs@groupes.renater.fr)

## OpenTheso

_Opentheso_ est un logiciel libre de gestion de thésaurus multilingue. Il peut être utilisé dans divers contextes : bases de données, système de gestion de bibliothèque, description de fonds d'archives, gestion bibliographique, système d'information géographique[^2].

_Opentheso_ est une solution collaborative de construction et de gestion de terminologie spécialisée ou de vocabulaire contrôlé. Il assure l'interopérabilité des terminologies utilisées (identifiants pérennes, conformité aux normes ISO et au modèle RDF).

Il est développé par la plateforme technologique [WST](https://www.mom.fr/plateformes-technologiques/web-semantique-et-thesauri) (Web Sémantique & Thesauri) située à la [Maison de l'Orient et de la Méditérranée](https://www.mom.fr/) de Lyon en partenariat avec le [GDS-FRANTIQ](http://www.frantiq.fr/).

**Demande d'ouverture d'un compte compte sur OpenThéso :** les demandes sont à adresser à [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr) en précisant le contexte : projet, type de thésaurus.

[^2]: Source [Wikipédia](https://fr.wikipedia.org/wiki/Opentheso)

!!! note
    - Voir l'instance _Opentheso_ hébergée par la TGIR Huma-Num : [opentheso.huma-num.fr](https://opentheso.huma-num.fr/opentheso/)
    - Voir la documentation d'_Opentheso_ : https://github.com/miledrousset/opentheso/wiki
    - Voir la présentation sur le site du Consortium MASA : [masa.hypotheses.org/opentheso](https://masa.hypotheses.org/opentheso)

## Stylo

_Stylo_ est un éditeur de texte scientifique simplifiant la rédaction et l'édition d'articles scientifiques en Sciences Humaines et Sociales.

_Stylo_ est développé par la [Chaire de recherche du Canada sur les écritures numériques](https://ecrituresnumeriques.ca), avec le soutien d'[Érudit](http://erudit.org/) et de [la TGIR Huma-Num](https://www.huma-num.fr/), sous licence [GPL-3.0](https://github.com/EcrituresNumeriques/stylo/blob/master/LICENSE).

_Stylo_ est désormais connecté à [HumanID](https://humanid.huma-num.fr/) et accessible à tous les utilisateurs et utilisatrices ayant un compte à HumanID. [Pour en savoir plus](https://humanum.hypotheses.org/6311).

!!! note
    - Accéder à l'instance _Stylo_ hébergée par la TGIR Huma-Num : [stylo.huma-num.fr](https://stylo.huma-num.fr)
    - Voir [la documentation de _Stylo_](http://stylo-doc.ecrituresnumeriques.ca/fr_FR/#!index.md)

## TXM

TXM est un service d'analyse de corpus de textes développé dans le cadre du projet [Textométrie](http://www.textometrie.org/) coordonné par l'UMR [IRHIM](http://ihrim.ens-lyon.fr) / ENS de Lyon, en partenariat avec l'EA [ELLIAD](http://elliadd.univ-fcomte.fr) / UFC.

Huma-Num opère l'ouverture de portails TXM sur des machines virtuelles hébergées sur son infrastructure.

**Demande d'ouverture de portails TXM :** Les demandes d'ouverture de portail sont à adresser à [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr) : l'utilisateur effectue une demande écrite précisant le contexte général de sa demande (projet
concerné et équipe de recherche). Huma-Num lui remet un formulaire décrivant l'organisation du support et la gestion du portail, ainsi que des informations qui sont à nous retourner.

Le support et la formation à l'utilisation de portails TXM sont assurés par l'IHRIM / ENS de Lyon.

TXM est un logiciel open-source, son code est disponible dans un [entrepôt Redmine](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/repository/show/tmp) de l'ENS de Lyon. 

!!! note
    - Voir l'exemple de portail TXM de démo hébergé par la TGIR Huma-Num : [txm-demo.huma-num.fr](https://txm-demo.huma-num.fr/txm)
    - Voir le [guide de prise en main rapide d'un portail TXM (Huma-Num)](https://sharedocs.huma-num.fr/wl/?id=zgMQUvhYex3T2aTqxH6PL1sXBGXUOUKp)
    - Participer à la liste de diffusion des administrateurs de portails TXM : [txm-admin@groupes.renater.fr](https://groupes.renater.fr/sympa/info/txm-admin)
    - Voir le point d'entrée de l'écosystème TXM (documentation du logiciel et tutoriels, listes de diffusion, wikis, autres versions du logiciel) : [textometrie.org](https://textometrie.org)

## Voyant Tools

Voyant Tools est un environnement en ligne de lecture et d’analyse de textes numériques développé par Stéfan Sinclair (†), McGill University et Geoffrey Rockwell, University of Alberta.

Huma-Num héberge l'un des 3 miroirs à l'international de cet outil. La traduction en français de l'interface a été réalisée par Aurélien Berra. Le code source est disponible sur [GitHub](https://github.com/sgsinclair/Voyant).

!!! note
    - Accéder à l'instance de Voyant Tools hébergée par la TGIR Huma-Num : [http://voyant.tools.huma-num.fr](http://voyant.tools.huma-num.fr)
    - Voir [la documentation de Voyant Tools](http://voyant.tools.huma-num.fr/docs/#!/guide)
