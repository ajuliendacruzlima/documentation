# Bienvenue sur la documentation des services de la TGIR Huma-Num

Vous trouverez ici la présentation et la documentation des services mis en place par la TGIR Huma-Num.

Pour en savoir plus sur la TGIR Huma-Num et son actualité, rendez-vous [sur son site web (huma-num.fr)](http://huma-num.fr) et sur  [son carnet de recherche (humanum.hypotheses.org)](https://humanum.hypotheses.org/).


## Comment contacter la TGIR Huma-Num :

- Vous souhaitez nous **faire part de besoins concernant votre projet ou demander directement l'ouverture de services** :
    - contactez par mail l'équipe Huma-Num : [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr)
    - les demandes d'accès à une partie des services sont directement accessibles sur [humanid](https://documentation.huma-num.fr/humanid/) (ShareDocs, GitLab, Matomo, Mattermost, NAKALA, ISIDORE, Stylo, Kanboard) : [https://humanid.huma-num.fr/](https://humanid.huma-num.fr/).

L'équipe prend soin d'analyser les demandes de manière collégiale sur les aspects scientifiques, documentaires, informatiques afin d'apporter une réponse adaptée à vos besoins tout en permettant de rationaliser et mutualiser les moyens mis en oeuvre. Selon la nature de la demande le temps de réponse peut varier entre 1 jour et 2 à 3 semaines.


- Vous rencontrez un **problème dans l'utilisation d'un service** délivré par Huma-Num : contactez [assistance@huma-num.fr](mailto:assistance@huma-num.fr)


- Pour **toute autre question** sur la TGIR Huma-Num : contactez [contact@huma-num.fr](mailto:contact@huma-num.fr)

### Quelles sont les conditions pour utiliser les services d'Huma-num :

Huma-Num délivre un ensemble de services et outils pour les données produites dans les projets de recherche en Sciences Humaines et Sociales : [https://www.huma-num.fr/presentation/#conditions](https://www.huma-num.fr/presentation/#conditions)

!!! note
    Cette documentation est en édition continue. Elle fait l'objet de [mises à jour régulières](https://gitlab.huma-num.fr/huma-num-public/documentation/-/commits/master). Vous pouvez y contribuer [en signalant un manque ou une erreur](https://gitlab.huma-num.fr/huma-num-public/documentation/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=), ou [en éditant vous-même la documentation (voir le protocole)](https://gitlab.huma-num.fr/huma-num-public/documentation/-/tree/master#contribuer)
