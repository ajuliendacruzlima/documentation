---
title: TXM
lang: fr
description: TXM, un service tiers hébergé par Huma-Num et lien vers sa documentation.
---

# TXM

!!! attention "À savoir"
    Les services tiers sont des outils et des plateformes dont la conception,
    la maintenance et la documentation sont assurés par  d'autres instances ou
    organisations que la TGIR Huma-Num. La TGIR Huma-Num met à disposition des
    machines virtuelles hébergées sur son infrastructure pour assurer l'accessibilité
    de ces outils et soutenir leur usage par la communauté SHS.

TXM est un service d'analyse de corpus de textes développé dans le cadre du projet [Textométrie](http://www.textometrie.org/) coordonné par l'UMR [IRHIM](http://ihrim.ens-lyon.fr) / ENS de Lyon, en partenariat avec l'EA [ELLIAD](http://elliadd.univ-fcomte.fr) / UFC.


Huma-Num opère l'ouverture de portails TXM sur des machines virtuelles
hébergées sur son infrastructure.

**Demande d'ouverture de portails TXM :** Les demandes d'ouverture de
portail sont à adresser à [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr) : l'utilisateur effectue
une demande écrite précisant le contexte général de sa demande (projet
concerné et équipe de recherche). Huma-Num lui remet un formulaire
décrivant l'organisation du support et la gestion du portail, ainsi que
des informations qui sont à nous retourner.

Le support et la formation à l'utilisation de portails TXM sont assurés
par l'IHRIM / ENS de Lyon.

TXM est un logiciel open-source, son code est disponible dans un [entrepôt Redmine](https://forge.cbp.ens-lyon.fr/redmine/projects/txm/repository/show/tmp) de l'ENS de Lyon. 

!!! note
    - Voir l'exemple de portail TXM de démo hébergé par la TGIR Huma-Num : [txm-demo.huma-num.fr](https://txm-demo.huma-num.fr/txm)
    - Voir le [guide de prise en main rapide d'un portail TXM (Huma-Num)](https://sharedocs.huma-num.fr/wl/?id=zgMQUvhYex3T2aTqxH6PL1sXBGXUOUKp)
    - Participer à la liste de diffusion des administrateurs de portails TXM : [txm-admin@groupes.renater.fr](https://groupes.renater.fr/sympa/info/txm-admin)
    - Voir le point d'entrée de l'écosystème TXM (documentation du logiciel et tutoriels, listes de diffusion, wikis, autres versions du logiciel) : [textometrie.org](https://textometrie.org)

