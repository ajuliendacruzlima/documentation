# Documentation Nakala

!!! attention "Information"
    Actuellement le module de publication web nommé Nakala_Press n'est pas encore documenté ci-dessous. Néanmoins ce service de Nakala a été présenté récemment lors de deux séminaires d'Huma-Num. Vous trouverez ces présentations dans la rubrique : Ressources (supports, vidéos).

## Introduction et présentation

### À quoi sert Nakala ?

Nakala est un service d’Huma-Num permettant à des chercheurs, enseignants-chercheurs et équipes de recherche de
partager, publier et valoriser tous types de données numériques documentées (fichiers textes, sons, images, vidéos,
objets 3D, etc.) dans un entrepôt sécurisé afin de les publier en accord avec les principes du _FAIR data_
(Facile à trouver, Accessible, Interopérable et Réutilisable).

![nakala_home](media/nakala/home.png){: style="width:500px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Interface de Nakala : https://www.nakala.fr/*

Nakala assure à la fois l’accessibilité aux données et aux métadonnées ainsi que leur "citabilité" dans le temps à
l’aide d’identifiants stables fournis par Huma-Num et basés sur Handle et/ou DOI.

Nakala s’inscrit dans le Web des données permettant notamment de rendre interopérable les métadonnées, c’est-à-dire la
possibilité de pouvoir les connecter à d’autres entrepôts existants suivant ainsi la logique des données ouvertes et
liées (*Linked Open Data*).

Par ailleurs, Nakala propose également un dispositif d’exposition des métadonnées qui permet leur référencement par des
moteurs de recherche spécialisés comme ISIDORE.

Nakala s’inscrit dans un dispositif cohérent de services mis en place par Huma-Num pour faciliter l’accès, le
signalement, la conservation et l’archivage à long terme des données de la recherche en SHS.

La description riche, précise et harmonisée de vos données avec Nakala permet à celles-ci d’être comprises sur le long
terme, de garantir leur traçabilité dans le temps et d’encadrer leur réutilisation.

### Dans quels cas utiliser Nakala ?

Il est intéressant d’utiliser Nakala dans les cas où l’on souhaite diffuser en ligne un ensemble de données et
métadonnées descriptives ayant une cohérence scientifique (corpus, collections, reportages, etc.).

Par exemple, [un fichier vidéo déposé dans Nakala](https://nakala.fr/11280/38d484fc) peut être inséré dans des pages
Web, comme dans le cas d’un [carnet de recherche Hypothèses](https://maisondescarnets.hypotheses.org/165) ou dans un
web-documentaire.

Plusieurs solutions s’offrent à vous pour exploiter les données qui sont dans Nakala :

- Vous utilisez le module de publication Nakala_Press proposé par Huma-Num ;
- Vous utilisez des outils existants comme un moteur de blogs, un CMS, etc. ;
- Vous avez les compétences techniques et vous développez votre site au-dessus des APIs de Nakala.

### Huma-Num prend en charge :

- Une copie sécurisée de vos données et de vos métadonnées sur son infrastructure ;
- L’attribution d’un identifiant stable pour permettre leur citation (Handle avant 2020 et DOI) ;
- La mise à disposition de vos métadonnées de manière interopérable en se basant sur les technologies du Web de données ;
- L'exposition des métadonnées des données par le protocole documentaire OAI-PMH ;
- La pérennité de l’interface web publique générée grâce au module de publication Nakala_Press.

### En résumé, que fait Nakala ?

- Il vous décharge de la gestion de vos données ;
- Il vous permet de les visualiser ;
- Il vous permet de les regrouper et les présenter dans des collections homogènes ;
- Il prend en charge le partage interopérable des données et des métadonnées et leur citabilité ;
- Il dissocie le stockage de données de leur présentation ;
- Il prépare le référencement des données dans ISIDORE et facilite le processus d’archivage à long terme ;
- Il permet l’éditorialisation de vos données dans un site web personnalisé de type *https://monprojet.nakala.fr* grâce
  au module de publication Nakala_Press.

### Que ne fait pas Nakala ?

- Il n’enrichit pas les données ;
- Il ne permet pas un stockage des données à caractères sensibles ou sous-droits.

## Accès et découverte 

### Demande de création d’un compte

L'accès à Nakala pour déposer et gérer des données nécessite de faire une demande d'accès sur le portail [HumanID](https://humanid.huma-num.fr/)

!!! info "Liens"
    - Le portail de connexion centralisée aux services d’Huma-Num HumanID : [https://humanid.huma-num.fr/](https://humanid.huma-num.fr/)
    - Comment se créer un compte sur [HumanID](https://documentation.huma-num.fr/humanid/)

Une fois connecté sur le portail HumanID, vous pouvez demander l'accès à Nakala.

Il est également possible de parcourir, rechercher et utiliser des données publiées de Nakala sans se connecter directement sur [https://www.nakala.fr/](https://www.nakala.fr/).

### Tester Nakala

Nakala dispose d’un environnement de test sur son interface web et ses API.

!!! info "Liens"
    - L'interface de test est disponible à l'adresse : [test.nakala.fr](https://test.nakala.fr/)
    - Les API de test sont disponibles à l'adresse : [apitest.nakala.fr](https://apitest.nakala.fr/)


!!! attention "À savoir"
    **Il n'est pas possible d'utiliser directement son compte HumanID personnel sur _test.nakala.fr_ et _apitest.nakala.fr_**. L'accès aux environnements de test se fait avec **l'un des quatre comptes disponibles** dont les login, mot de passe et clé d'API sont présentés directement sur la page d'accueil de _test.nakala.fr_.

Ces environnements sont indépendants de la version de production _www.nakala.fr_.
Pour toute utilisation de Nakala en test, nous vous recommandons d’utiliser les environnements de test. En effet, dans ces environnement les données déposées restent en interne et peuvent être réellement définitivement supprimées. Contrairement à l'environnement de production où toute donnée déposée se voit attribuer un DOI et dont il restera une trace même après suppression.

!!! attention "À savoir"
    Noter que les environnements de test sont remis à vide et les données supprimées chaque lundi matin.

## Déposer des données

### Accueil et tableau de bord

Une fois votre compte créé et votre accès au service validé, vous pourrez accéder aux différentes fonctionnalités de
Nakala. Pour cela, vous vous connectez à Nakala avec le bouton "se connecter" en haut à droite de l'écran.

- Un onglet **Tableau de bord** permet de suivre les métriques de vos données de façon chiffrée : nombre de données
  déposées/publiées, consultations de ces données, téléchargements, stockage privé, fréquence des dépôts. Cette page
  vous est personnelle ;
- Un onglet **Données** permet de lister vos données ou des données que d’autres utilisateurs ont partagées avec vous ;
- Un onglet **Collections** permet de gérer vos différentes collections ainsi que celles partagées avec vous par
  d’autres utilisateurs ;
- Un onglet **Listes** permet la gestion de vos groupes d’utilisateurs afin de gérer plus facilement les droits d’accès
  et de contribution à vos dépôts ;
- Un onglet **Sites web** permet la gestion de vos différents sites Nakala_Press.

![nakala_dashboard](media/nakala/dashboard.png){: style="width:600px; border: 1px solid grey; display: block; margin-left: auto; margin-right: auto"}

*Vue de l'onglet tableau de bord de Nakala*

### Organisation des données en collections

Les données déposées dans Nakala peuvent être regroupées dans des collections. Une collection regroupe un ensemble de
données cohérentes. Vous pouvez ainsi avoir plusieurs collections, correspondant à différents projets de recherche et
comprenant différents contributeurs ayant des droits spécifiques au sein de ces collections. 

Une donnée peut appartenir à plusieurs collections.

Il n’y a pas de hiérarchie entre les collections.

#### Création d'une collection

Pour créer une collection, depuis le tableau de bord, rendez-vous sur l'onglet collection, bouton "créer une nouvelle collection" en haut à droite de l'écran. Vous avez à choisir :

- Si celle-ci est privée ou publique (métadonnée obligatoire) ;
- Son ou ses titres en précisant ou non leur langue (métadonnée obligatoire) ;
- Des informations complémentaires et optionnelles sur la description de cette collection, ses mots-clés, etc. ;
- Les droits afférents à cette collection et le rôle des contributeurs associés à celle-ci. À la suite de l’ajout d’un
  utilisateur ou d’une liste d’utilisateurs, un petit oeil vous permet de donner à l’utilisateur de la collection le
  rôle d’**administrateur**, d’**éditeur** ou de **lecteur**.

#### Partage des droits avec d'autres utilisateurs 
Pour faciliter l’ajout de membres, il est possible de créer des listes d’utilisateurs (appartenant au même projet, par
exemple) dans l’onglet **Listes** et d’ajouter ensuite directement cette liste à la collection. De même, dans les
onglets **Collections** et **Données**, la fonctionnalité **Partagées avec moi** permet d’identifier quelles données ou
collections l’utilisateur a en partage.

#### Gestion des droits
Une collection **publique** ne peut contenir que des données **publiées**. Vous n’êtes pas obligés d’avoir les droits d’administration ou de lecture sur une donnée publiée pour pouvoir l’ajouter dans votre collection.

Les droits de gestion d’une collection sont indépendants des droits de gestion de données qu’elle contient. Cela
signifie que si vous donnez les droits de lecture à un utilisateur sur une collection qui contient des données non
publiées, vous devrez aussi donner à ce même utilisateur les droits de lecture sur les données si vous souhaitez qu’il
puisse y avoir accès.

#### Collections et SET OAI Nakala
Toute collection publique constitue un set dans l'entrepôt OAI-PMH de Nakala. Ce set OAI peut, en demandant à [isidore-sources@huma-num.fr](mailto:isidore-sources@huma-num.fr), être indexé par le moteur ISIDORE et être présenté comme une collection dans cette plateforme.

### Dépôt de données

Une donnée Nakala est constituée d'un ou plusieurs fichiers et de métadonnées descriptives.
Le dépôt d'une donnée dans Nakala peut se faire de deux façons différentes :

#### Formulaire de dépôt en ligne

Connecté à votre compte, sur le tableau de bord, allez sur le bouton en haut à droite de l’interface web "Déposer".
Le formulaire de dépôt s'affiche pour charger le ou les fichiers, compléter les métadonnées et configurer le dépôt (droits d'accès, collections etc.)

![Depot](media/nakala/submit.png){: style="width:600px; border: 1px solid grey;"}

*Vue du formulaire de dépôt de Nakala*

#### Dépôt par l'API de Nakala

Dans le cas du dépôt de données par l'API de Nakala, chaque fichier de donnée associé aux métadonnées est à déposer avant via `POST /uploads` puis les métadonnées afférentes déposées grâce à l’onglet `POST /datas`.
  
API de Nakala :

- [API de production](https://api.nakala.fr/doc)
- [API de test](https://apitest.nakala.fr/doc)

![Depot2](media/nakala/api-upload.png){: style="width:600px; border: 1px solid grey;"}

*API de Nakala : chargement de fichier(s)*

![Depot3](media/nakala/api-data.png){: style="width:600px; border: 1px solid grey;"}

*API de Nakala : dépôt de la/les donnée(s)*

#### Dépôt par lot
Le dépôt par lot n'est pas possible depuis le formulaire de dépôt en ligne. (Il est par contre possible de déposer plusieurs fichiers pour une même données). _Cette fonctionnalité est en cours de développement pour l’interface web._
Le dépôt par lot est possible actuellement par l'API de Nakala.

### Visibilité des fichiers et délai (embargo)

Par défaut, les fichiers sont visibles dès le dépôt de la donnée. 

Il est possible de différer la date de visibilité du/des fichier(s) de cette donnée. La notice de métadonnées est toujours visible. Pour ajouter un délai de visibilité, éditer le fichier en cours de dépôt et sélectionner la date de visibilité, ou jamais. Le ou les fichiers concernés ne seront rendus publics qu'à l'issue de ce délai.

Lorsqu’un fichier est placé sous cette limitation, seuls les utilisateurs ayant au minimum les droits "lecteur" sur la donnée pourront accéder à ce fichier.

![Embargo](media/nakala/embargo.png){: style="width:600px; border: 1px solid grey;"}

*Formulaire de dépôt dans Nakala : appliquer un délai de visibilité au(x) fichier(s)*

### Distinction données déposées / données publiées

Après avoir dûment rempli les métadonnées correspondantes aux données, l’utilisateur a la possibilité de **déposer** ses
données ou de les **publier**.

- Le **dépôt** rend visible la donnée et ses fichiers uniquement aux utilisateurs ayant un droit d’accès dessus. Il
  s’agit d’un état transitoire avant publication définitive (ex: étape de relecture). Chaque utilisateur dispose d’un
  espace de stockage limité pour ces données non-publiées (1000 données ou 100 Go). Une donnée non publiée ne peut pas appartenir à une
  collection publique et ne peut pas être éditorialisée dans un site Nakala_Press. Une donnée déposée peut être supprimée.
- La **publication** rend la donnée accessible et réutilisable par tous. Le **DOI est publié** chez Datacite. Il n’y a pas de limite de stockage spécifique
  pour les données publiées. Une donnée peut être publiée avec des fichiers sous embargo si seules les métadonnées
  doivent être accessibles librement. La publication d’une donnée est définitive. Il **n’est pas possible de supprimer** ou
  de dépublier une donnée qui a été publiée à moins de faire une demande spécifique à l’équipe d’Huma-Num.

### Description des données / métadonnées descriptives

!!! attention "À savoir"
    La rubrique [guide de description](/nakala-guide-de-description/) de cette documentation fournit des conseils sur les modalités de description. 

Le format de métadonnées dans Nakala est le [Dublin-Core qualifié](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/).

Cinq métadonnées propres à Nakala sont également obligatoires pour déposer et publier une donnée :

- **Titre** (nakala:title, multivaluée)
- **Type** (nakala:type, unique)
- **Auteur** (nakala:creator, multivaluée)
- **Date** (nakala:created, unique)
- **Licence** (nakala:license, unique)

Ces métadonnées doivent être exprimées au moment du dépôt dans le vocabulaire Nakala, mais peuvent être converties en
DublinCore au moment du requêtage des données via l’API ou via le protocole OAI-PMH.

#### Les métadonnées descriptives 
- Titre : il est possible d’ajouter plusieurs titres nakala:title en précisant ou non la langue de chacun. Nakala propose une liste de plus de 7000 langues vivantes ou éteintes selon les normes ISO-639-1 et ISO-639-3.

- Auteur : Nakala propose en autocomplétion la base des auteurs en fonction des dépôts effectués par les utilisateurs. Si un auteur n’est pas disponible dans l’autocomplétion proposée depuis l’interface web, il est possible d’en ajouter un nouveau en
cliquant sur "Ajouter d’autres auteurs" en bas du résultat de l’autocomplétion. Chaque auteur peut être décrit par son
nom, son prénom et éventuellement un identifiant [ORCID](https://orcid.org/). Lorsque l’auteur ne peut pas être
renseigné, il est possible de cocher la case "Anonyme" via l’interface web ou de renseigner une valeur à *null* via l’API.
Il est possible de renseigner d’autres formes auteurs en ajoutant des métadonnées issues du vocabulaire DublinCore.

- Date : le format de date attendu pour nakala:created est de type AAAA, AAAA-MM ou AAAA-MM-JJ. Lorsque la date est inconnue ou ne peut pas être renseignée, il est possible de cocher la case "Inconnue" via l’interface web ou de renseigner une valeur à *null* via
l’API. Il est possible d’indiquer d’autres formats de date en ajoutant par exemple une métadonnée dans le vocabulaire
DublinCore.

- Licence : Le référentiel des licences contient actuellement plus de 400 valeurs. Vous pouvez proposer à l’équipe d’Huma-Num de
nouvelles valeurs pour compléter cette liste si besoin.

#### Les types de données dans Nakala
Les types des données Nakala sont identiques à ceux utilisés dans le moteur de recherche [ISIDORE](https://documentation.huma-num.fr/isidore/). La liste est disponible dans le tableau ci-dessous
ou via l'API à l'URL <https://api.nakala.fr/vocabularies/datatypes>.


| Type | URI |
| :--- | :---|
| image|<http://purl.org/coar/resource_type/c_c513> |
| video|<http://purl.org/coar/resource_type/c_12ce> |
| son|<http://purl.org/coar/resource_type/c_18cc> |
| publication|<http://purl.org/coar/resource_type/c_6501> |
| poster|<http://purl.org/coar/resource_type/c_6670> |
| présentation|<http://purl.org/coar/resource_type/c_c94f> |
| cours|<http://purl.org/coar/resource_type/c_e059> |
| livre|<http://purl.org/coar/resource_type/c_2f33> |
| carte|<http://purl.org/coar/resource_type/c_12cd> |
| dataset|<http://purl.org/coar/resource_type/c_ddb1> |
| logiciel|<http://purl.org/coar/resource_type/c_5ce6> |
| autres|<http://purl.org/coar/resource_type/c_1843> |
| fonds d'archives|<http://purl.org/library/ArchiveMaterial> |
| exposition d'art|<http://purl.org/ontology/bibo/Collection> |
| bibliographie|<http://purl.org/coar/resource_type/c_86bc> |
| bulletin|<http://purl.org/ontology/bibo/Series> |
| édition de sources|<http://purl.org/coar/resource_type/c_ba08> |
| manuscrit|<http://purl.org/coar/resource_type/c_0040> |
| correspondance|<http://purl.org/coar/resource_type/c_0857> |
| rapport|<http://purl.org/coar/resource_type/c_93fc> |
| périodique|<http://purl.org/coar/resource_type/c_2659> |
| prépublication|<http://purl.org/coar/resource_type/c_816b> |
| recension|<http://purl.org/coar/resource_type/c_efa0> |
| partition|<http://purl.org/coar/resource_type/c_18cw> |
| données d'enquêtes|<https://w3id.org/survey-ontology#SurveyDataSet> |
| texte|<http://purl.org/coar/resource_type/c_18cf> |
| thèse|<http://purl.org/coar/resource_type/c_46ec> |
| page web|<http://purl.org/coar/resource_type/c_7ad9> |
| data paper|<http://purl.org/coar/resource_type/c_beb9> |
| article programmable|<http://purl.org/coar/resource_type/c_e9a0> |



Ces URI sont par ailleurs alignées avec l'ontologie des types ISIDORE (dont les labels sont en anglais, français et espagnol) et peut être obtenu en requêtant [le 3store d'ISIDORE](https://isidore.science/sqe) avec la SPARQL [suivante](https://isidore.science/sparql?default-graph-uri=&query=SELECT+*+WHERE+%7B%0D%0A%3FTypeISIDORE+skos%3AinScheme+%3Chttp%3A%2F%2Fisidore.science%2Fontology%3E.%0D%0A%3FTypeISIDORE+skos%3AprefLabel+%3FLabelISIDORE.%0D%0A%3FTypeISIDORE+owl%3AequivalentClass+%3FTypeCOARpourNAKALA%0D%0A%0D%0A%7D&format=text%2Fhtml&timeout=0&debug=on) :
```
SELECT * WHERE {
?TypeISIDORE skos:inScheme <http://isidore.science/ontology>.
?TypeISIDORE skos:prefLabel ?LabelISIDORE.
?TypeISIDORE owl:equivalentClass ?TypeCOARpourNAKALA
}
```


#### l'indexation par mots-clés dans Nakala

La métadonnée "Mots-clés", associée au vocabulaire dcterms:subject, est liée aux différents thesaurus utilisés dans [ISIDORE](https://isidore.science/vocabularies).
En effet, Nakala propose ici en autocomplétion tous les labels (prefLabel et altLabel) des concepts des référentiels qu'utilisent ISIDORE
pour enrichir ses données : le vocabulaire Rameau, les thésaurus Pactols, GEMET, LCSH, BNE, GéoEthno, Archires et enfin le référentiel géographique Geonames.

![meta_subject](media/nakala/meta_subject.png){: style="width:600px; border: 1px solid grey;"}

Aujourd'hui, seul le label du concept est enregistré dans les métadonnées de la donnée. C'est donc au déposant d'ajouter
les labels dans plusieurs langues si nécessaire.

!!! Note
    Une évolution prévue de Nakala est d'enregistrer au lieu du label, l'objet concept lui-même et d'afficher ou exporter ensuite toutes les informations du concept (labels, liens, ...).

Lors du dépôt, la donnée peut être intégrée à une ou plusieurs collections en respectant la contrainte qu'une collection
publique ne peut regrouper que des données publiées.

![in-collection](media/nakala/in-collections.png){: style="width:600px; border: 1px solid grey;"}

### Attribution d’un identifiant pérenne : DOI.

Nakala fournit automatiquement un identifiant unique et stable à chaque donnée déposée, un DOI. Il n’y a donc rien à
faire de particulier pour obtenir un identifiant avec Nakala. Dès qu’une donnée est publiée, les droits de
lecture sont remontés dans le moteur de recherche, le serveur OAI-PMH et publiés dans Datacite.

Vous pourrez accéder à votre donnée par un lien constitué de la manière
suivante : ``https://nakala.fr/*identifiant*``

Par exemple si la donnée a pour identifiant 11280/000028fb, le lien pour accéder ou citer la donnée
est : <https://nakala.fr/11280/000028fb>.

## L'accès aux données déposées dans Nakala

### Modalités d'accès possibles

L’accès à Nakala se fait via l’adresse <https://nakala.fr>.

Nakala propose, lorsque l’utilisateur n’est pas connecté, une page d’exploration et de recherche
des données permettant de les visualiser et de les réutiliser. Par exemple : <https://nakala.fr/11280/d6dfc55a>

La page de présentation d’un objet est <https://nakala.fr/{identifiant}>, où {identifiant}
correspond à un Handle ou un DOI.

Les URLs des collections sont accessibles sur <https://nakala.fr/collection/{identifiant}>.

Nakala dispose de son propre entrepôt OAI-PMH dont l’adresse Web est : <https://api.nakala.fr/oai2>. Chaque collection
de Nakala correspond à un set OAI. Ainsi, tous les utilisateurs de l’espace Nakala peuvent utiliser l’adresse Web de
l’entrepôt OAI-PMH pour faire moissonner les métadonnées par des portails
documentaires ([ISIDORE](https://isidore.science/), [Gallica](https://gallica.bnf.fr/), etc.).

### Les rôles dans Nakala

Les rôles d'un utilisateur sur une donnée ou une collection sont :

- Déposant (`ROLE_DEPOSITOR`) ;
- Propriétaire (`ROLE_OWNER`) ;
- Administrateur (`ROLE_ADMIN`) ;
- Éditeur (`ROLE_EDITOR`) ;
- Lecteur (`ROLE_READER`) ;
- Anonyme (GUEST).

#### Droits sur les données

Ces rôles permettent d'effectuer les actions suivantes sur une donnée :

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'une donnée publiée | + | + | + | + | + |
| Consultation d'une donnée déposée | + | + | + | + | - |
| Consultation d'une ancienne version (1)| + | + | + | + | + |
| Consultation d'une donnée supprimée (2)| + | + | + | + | + |
| Modification des métadonnées d'une donnée | + | + | + | - | - |
| Modification des métadonnées d'une donnée supprimée ou ancienne version | - | - | - | - | - |
| Publication d'une donnée déposée (3) | + | + | - | - | - |
| Modification des droits d'accès à une donnée (4) | + | + | - | - | - |
| Suppression d'une donnée déposée | + | + | - | - | - |
| Suppression d'une donnée publiée (5) | - | - | - | - | - |
| Suppression d'une ancienne version | - | - | - | - | - |
| Suppression d'une donnée supprimée | - | - | - | - | - |

1. Il s'agit de la consultation d'une ancienne version d'une donnée publiée
2. Seules certaines informations sont consultables (ex: titre, date de la suppression, déposant)
3. Pour être publiée, une donnée doit avoir un certain nombre de métadonnées obligatoires (`nakala:title`, `nakala:creator`, `nakala:created`, `nakala:type`, `nakala:license`)
4. Contrairement au `ROLE_ADMIN`, le `ROLE_OWNER` ne peut pas être supprimé.
5. La suppression d'une donnée publiée ne peut se faire que sur demande aux administrateurs de Nakala.


#### Droits sur les fichiers composant une donnée

Les droits associées aux fichiers d'une donnée dépendent des critères suivants :
 
- le rôle de l'utilisateur sur la donnée (cf. plus haut)
- le statut de la donnée (déposée, publiée, ancienne version, supprimée)
- la présence ou non d'une date d'embargo en cours sur le fichier

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'un fichier ouvert d'une donnée publiée | + | + | + | + | + |
| Consultation d'un fichier sous embargo d'une donnée publiée | + | + | + | + | - |
| Consultation d'un fichier d'une donnée déposée (indépendant de son embargo) | + | + | + | + | - |
| Consultation d'un fichier ouvert d'une ancienne version d'une donnée | + | + | + | + | + |
| Consultation d'un fichier sous embargo d'une ancienne version d'une donnée | + | + | + | + | - |
| Consultation d'un fichier d'une donnée supprimée (indépendant de son embargo) | - | - | - | - | - |
| Modification des fichiers associés à une donnée déposée ou publiée | + | + | + | - | - |
| Modification des fichiers associés à une ancienne version ou à une donnée supprimée | - | - | - | - | - |


#### Droits sur les collections

Les droits associés à une collection dépendent des critères suivants :

- le rôle qu'a l'utilisateur sur la collection (`ROLE_OWNER`, `ROLE_ADMIN`, `ROLE_EDITOR`, `ROLE_READER`, GUEST)
- le statut de la collection (privée, publique)

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'une collection publique | + | + | + | + | + |
| Consultation d'une collection privée | + | + | + | + | - |
| Modification des métadonnées | + | + | + | - | - |
| Publication d'une collection privée (1) | + | + | - | - | - |
| Dépublication d'une collection publique | + | + | - | - | - |
| Modification des droits d'accès à une collection (2) | + | + | - | - | - |
| Suppression d'une collection (indépendamment de son statut) | + | + | - | - | - |

1) Pour pouvoir être publiée, une collection ne doit contenir que des données déjà publiées
2) Contrairement au `ROLE_ADMIN`, le `ROLE_OWNER` ne peut pas être supprimé.

Les possibilités d'ajout d'une donnée dans une collection dépendent des critères suivants :

- le statut de la donnée (déposée, publiée, ancienne version, supprimée)
- le rôle d'un utilisateur sur la collection (`ROLE_OWNER`, `ROLE_ADMIN`, `ROLE_EDITOR`, `ROLE_READER`, GUEST)
- le statut de la collection (privée, publique)

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Ajout ou suppression d'une donnée publiée dans une collection | + | + | + | - | - |
| Ajout ou suppression d'une donnée déposée dans une collection privée | + | + | + | - | - |
| Ajout ou suppression d'une donnée déposée dans une collection publique | - | - | - | - | - |
| Ajout ou suppression d'une ancienne version d'une donnée supprimée dans une collection (indépendamment de son statut) | - | - | - | - | - |


#### Droits sur les listes d'utilisateurs

Les droits sur les listes d'utilisateurs dépendent des critères suivants :

- le propriétaire de la liste
- les personnes appartenant à la liste

|Actions|Propriétaire|Membre|Guest|
| :-----| :-----: | :------: | :------: |
| Ajout ou suppression d'une personne dans une liste | + | - | - |
| Renommage d'une liste | + | - | - |
| Voir les membres d'une liste | + | + | - |
| Utiliser la liste lors de l'attribution de droits sur une donnée ou une collection | + | + | - |


#### Droits sur les sites Nakala_PRESS

Les droits sur les sites Nakala_PRESS dépendent des critères suivants :

- les droits sur la collection associée au site web (`ROLE_OWNER`, `ROLE_ADMIN`, `ROLE_EDITOR`, `ROLE_READER`, Guest)
- le statut du site web (publié ou non)

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'un site publié | + | + | + | + | + |
| Consultation d'un site non publié | + | + | - | - | - |
| Édition de la configuration d'un site | + | + | - | - | - |
| Envoi d'un fichier pour l'édition d'un site | + | + | - | - | - |
| Publication d'un site | + | + | - | - | - |
| Suppression d'un site | + | + | - | - | - |


### Connexion avec Zotero

Une connexion entre Nakala et [Zotero](https://www.zotero.org/) est proposée et vous permet grâce au module web de Zotero
de référencer une donnée de Nakala dans votre gestionnaire bibliographique avec toutes les métadonnées correspondantes.

### Suppression des données et archivage

Nakala n’offre pas la possibilité de supprimer directement une donnée publiée. Lorsque les fichiers d’une donnée publiée
sont modifiés, une nouvelle version de la donnée est générée et toutes les anciennes versions restent accessibles. La
modification uniquement des métadonnées n’entraine pas la création d’une nouvelle version. Si l’utilisateur souhaite
qu'une donnée qu'il a publiée soit supprimée, il doit en faire la demande à <nakala@huma-num.fr>. Le DOI de la donnée sera
cependant toujours conservé, même si elle est notée comme supprimée.

Les données déposées dans Nakala n’entrent pas automatiquement dans un processus d’archivage à long terme. Pour mettre
en place ce processus, vous devez prendre contact avec les membres d’Huma-Num via <cogrid@huma-num.fr>.

### Si Huma-Num disparaît, comment sera assurée la citabilité des données déposées dans Nakala ?

Comme Nakala utilise le système de nommage DOI pour attribuer un identifiant unique à chaque donnée, il sera toujours
possible de transférer la base des identifiants à toute autre structure qui aura en charge la suite du dispositif (par exemple la BnF, CINES, Archives Nationales, etc).

## Ressources (supports, vidéos)

### Rencontres Huma-Num 2021
**Présentations de Nakala lors des Rencontres Huma-Num 25-28 mai 2021**

- [Programme complet]( https://rhn2020.sciencesconf.org/program/details)
- [Présentation de Nakala](https://rhn2020.sciencesconf.org/data/program/nkp_rhn2021_LaurentCapelli.pdf)
- [Dépôt dans Nakala](https://rhn2020.sciencesconf.org/data/program/presentation_deposer_dans_nakala.pptx_BrunoVAnderaert.pdf)
- [Présentation du module Nakala_Press de publication](https://rhn2020.sciencesconf.org/data/program/nkp_rhn2021_LaurentCapelli_1.pdf)

### ANF Huma-Num 2021
**Présentations de Nakala lors de l'ANF "gérer ses données en SHS avec les services et outils proposés par la TGIR Huma-Num" septembre 2021**

- [Programme complet](https://anf2021-humanum.sciencesconf.org/resource/page/id/1)
- [Présentation générale, fonctionnalités, dépôt, collections, droits, recherche](https://anf2021-humanum.sciencesconf.org/data/pages/ANF2021_module_3.pdf)
- [API de Nakala, organisation de la documentation, authentification, verbes et TP de dépot en lot](https://anf2021-humanum.sciencesconf.org/data/pages/ANF2021_modules_3_et_4.pdf)
- [Présentation du module Nakala_PRESS et TP](https://anf2021-humanum.sciencesconf.org/data/pages/nkp_anf_2022.pdf)
- [Présenter ses données en dehors de Nakala](https://anf2021-humanum.sciencesconf.org/data/pages/ANF_HN_2021_Module_6.pdf)
