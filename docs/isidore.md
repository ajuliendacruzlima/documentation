---
lang: fr
description: Présentation d'ISIDORE, le moteur de recherche permettant de trouver des publications, des données numériques et profils de chercheurs et chercheures en sciences humaines et sociales venant du monde entier.
---

# ISIDORE

## Qu’est-ce qu’ISIDORE ?

ISIDORE est un moteur de recherche permettant de découvrir et de trouver des publications, des données numériques et profils de chercheurs et chercheures en sciences humaines et sociales (SHS) venant du monde entier.

Il permet de rechercher dans le texte intégral de plusieurs millions de documents (articles, thèses et mémoires, rapports, jeux de données, pages Web, notices de bases de données, description de fonds d’archives, etc.), des signalements événements (séminaires, colloques, etc.). De plus, ISIDORE relie entre eux ces millions de documents en les enrichissants à l'aide de concepts scientifiques issus des travaux des communautés de recherche des SHS.

Il est accessible sur le Web sur le portail [isidore.science](https://isidore.science).

Il propose également des fonctionnalités de réseau social scientifique. À ce titre, il entre dans la catégorie des moteurs et assistants de recherche et offre de nombreuses fonctionnalités pour organiser de la veille scientifique.

Lancé le 8 décembre 2010, ISIDORE est le fruit de la collaboration du "très grand équipement" Adonis du CNRS (2007-2013), du Centre pour la communication scientifique directe et des sociétés Antidot, Mondéca et Sword. Il est actuellement développé, mise à jour et exploité par la TGIR Huma-Num.

Références sur l'histoire d'ISIDORE :

- POUYLLAU, Stéphane, CAPELLI, Laurent, MINEL, Jean-Luc, BUNEL, Mélanie, SAURET, Nicolas, BAUDE, Olivier, JOUGUET, Hélène, BUSONERA, Pauline, & DESSEIGNE, Adrien. (2021). ISIDORE à 10 ans. Zenodo. [10.5281/zenodo.5699997](https://doi.org/10.5281/zenodo.5699997)
- Philippe Bourdenet, "L'espace documentaire en restructuration : l'évolution des services des bibliothèques universitaires", Le serveur TEL (thèses-en-ligne), [10670/1.lnieuv](https://isidore.science/document/10670/1.lnieuv)
- Yannick Maignien, "ISIDORE, de l'interconnexion de données à l'intégration de services", Hyper Article en Ligne - Sciences de l'Homme et de la Société, [10670/1.k9lck9](https://isidore.science/document/10670/1.k9lck9)
- Stéphane Pouyllau et al., "Bilan 2011 de la plateforme ISIDORE et perspectives 2012-2015", MoDyCo, Modèles, Dynamiques, Corpus - UMR 7114, [10670/1.bqexsj](https://isidore.science/document/10670/1.bqexsj)

## Comment fonctionne ISIDORE ?

ISIDORE moissonne des métadonnées textuelles et du texte intégral, les enrichit puis les indexe. Il exploite les métadonnées des documents ainsi que le texte intégral, le but est d'analyser ces informations afin de les enrichir, de les relier des concepts des référentiels scientifiques (thésaurus, etc.), de les relier aux identifiants des auteurs (ORCID, IDRef, IDHAL, VIAF, etc.).

Plusieurs enrichissements sont effectués :

- L'annotation sémantique : les mots présents dans les métadonnées des documents sont comparés aux entrées des référentiels par le biais d'un algorithme fondé sur une analyse morphologique des termes. Si une équivalence s'effectue entre un terme issu du document une entrée de l'un des référentiels, alors la ressource sera reliée à ladite entrée du référentiel. Les référentiels sont multilingues et alignés entre eux. Ainsi, l'annotation sémantique est multilingue.

- La catégorisation disciplinaire : ISIDORE utilise un classifieur sémantique qui, après avoir été entrainé sur un corpus de référence, catégorise dans les disciplines SHS du référentiel MORESS, tous les documents présents dans ISIDORE. L'entrainement du classifieur est réalisé à l'aide de la catégorisation manuelle réalisé par les chercheurs dans HAL lors du dépôt de leurs publications.

- La détection des auteurs : ISIDORE détecte les auteurs des documents et enrichit la forme auteur (prénom et nom) à l'aide d'identifiants auteurs internationaux (ORCID, VIAF, ISNI) et nationaux (IDHAL, IDRef).

ISIDORE indexe, dans son moteur de recherche :

- Les métadonnées des documents ;
- Le texte intégral (s'il est disponible en libre accès) ;
- Les annotations sémantiques ;
- La classification disciplinaire ;
- L'enrichissement et la normalisation des auteurs.

Plus d'information est disponible sur [la page "Référentiels"](https://isidore.science/vocabularies) d'ISIDORE.

### ISIDORE peut-il indexer des documents et données multilingues ?

Oui. Depuis 2015, les documents et jeux de données en anglais, espagnol
et français sont indexés, enrichis et reliés aux référentiels scientifiques par ISIDORE (métadonnées et texte intégral). Pour le texte intégral hors de ces trois langues, il est indexé dans la langue du document mais l'enrichissement n'a pas lieu.
Pour plus d’information, vous pouvez consulter notre billet sur le sujet : [Isidore speaks English, sino también español et toujours en français](https://humanum.hypotheses.org/921).

### Quelle est la fréquence de mise à jour d’ISIDORE ?

ISIDORE est mis à jour, de façon incrémentale, en moyenne une fois par
mois. Pourquoi ce délai ? En plus de moissonner et d’indexer les
documents, ISIDORE les enrichit à l’aide de concepts issus de
référentiels scientifiques (thésaurus, taxonomie, etc.). Ce travail d’enrichissement sémantique est
automatique et permet de vous proposer des suggestions de lecture. Il
s’agit de vous faire découvrir des documents autres que ceux que vous
cherchiez. Cela nécessite un certain temps de traitement et de calcul.
Les mises à jour des documents vous concernant, qui vous seront ainsi
proposés dans votre compte utilisateur comme des documents à
revendiquer, suivront elles aussi ce rythme mensuel de mise à jour.

L'historique des mises à jour d'ISIDORE est disponible sur [https://isidore.science/releases](https://isidore.science/releases).

### Quel est le circuit d'ajout de collections dans ISIDORE ?

Deux cas de figure :

- Un projet de recherche, une équipe, un laboratoire, une bibliothèque peuvent proposer des collections à moissonner par simple e-mail à <isidore-sources@huma-num.fr>. L'équipe d'Huma-Num étudie la demande et échange avec le demandeur afin de bien comprendre comment sont décrites les métadonnées et les données à indexer. Le plus souvent il est procédé à un premier moissonnage et une première indexation et enrichissement pour que le demandeur puisse voir et analyser comment seront indexées ses données dans ISIDORE. Puis, les échanges se poursuivent potentiellement pour ajuster au mieux le processus d'indexation.

- L'équipe d'Huma-Num repère un entrepôt de données ou une bibliothèque numérique et prend contact avec le producteur des données ou la structure qui diffuse ces données pour échanger et proposer le moissonnage et l'indexation dans ISIDORE. Il est procédé à un premier moissonnage et une première indexation et enrichissement pour que le demandeur puisse voir et analyser comment seront indexées ses données dans ISIDORE. Puis, les échanges se poursuivent potentiellement pour ajuster au mieux le processus d'indexation.

## Comment utiliser ISIDORE ?

ISIDORE propose plusieurs outils pour rechercher, découvrir, collecter et organiser les contenus qu’il indexe :

### Le portail isidore.science

Le portail [isidore.science](https://isidore.science) est un site Web en trois langues qui propose un [moteur de recherche par pertinence](https://isidore.science) qui peut être utilisé avec plusieurs méthodes d’interrogation.

-   Par défaut, ISIDORE cherche tous les mots d’une requête posée par
    l’utilisateur/utilisatrice en enlevant les mots vides ("de", "la", "le",
    "les", etc.) ;
-   Il est possible de chercher un document avec une phrase complète ou
    un groupe de mots en utilisant les guillemets autour de la phrase,
    par exemple : "direction de conscience" cherchera précisément
    cette phrase. Ainsi, dans ce cas-là, le "de" ne sera pas considéré
    comme un mot vide ;

#### Opérateurs de recherche
Plusieurs opérateurs de recherche booléens sont disponibles dans
ISIDORE. À noter que la syntaxe des opérateurs est importante dans
ISIDORE, ils sont toujours en MAJUSCULE (ex. ET ou AND) :

- ET (AND) : l’intersection permet de trouver les termes (ou ensemble
    de termes) communs à la requête. Par exemple :
    -   conscience ET genre
    -   "guerre froide" ET migrations
- OU (OR) : la réunion permet de trouver les termes cherchés
    appartenant aux deux ensembles de termes, ou à l’un ou à l’autre.
    Par exemple :
    -   "Web sémantique" OU "Web 3.0"
- SAUF (NOT) : l’exclusion permet de réduire le bruit en excluant des
    termes. Par exemple :
    -   révolution SAUF Française
- PROCHE(n.) (NEAR(n.)) : l’opérateur PROCHE(n.) (comprendre "proche
    de") permet de lier des termes en indiquant une valeur "n." de
    proximité entre ces derniers. Il fonctionne comme un ET avec n.
    mot(s) entre les termes. La valeur "n." indique le nombre de mots
    devant séparer les deux termes recherchés. PROCHE fonctionne aussi
    sans la valeur n. et est dans ce cas-là égal à un PROCHE(10), c’est
    à dire 10 mots entre les termes recherchés (espacement standard).
    -   maison PROCHE(4) noblesse : recherche maison et noblesse avec
        une proximité de 4 mots

#### Tri des résultats de recherche

Par défaut, dans [isidore.science](https://isidore.science), il est proposé un tri des résultats par pertinence sémantique. Il est possible de changer le tri des résultats de recherche pour :

- un tri par nouveauté
- un tri sur le nom de l'auteur·e par ordre alphabétique
- un tri sur le nom de l'auteur·e par ordre alphabétique inversé
- un tri par date croissante
- un tri par date décroissante

Très prochainement, sera disponible de nouveau :

- un tri sur le titre par ordre alphabétique
- un tir sur le titre par ordre alphabétique inversé

### La recherche avancée

Une recherche avancée est également disponible à l’adresse [https://isidore.science/as](https://isidore.science/as) et également accessible depuis
la première page du [portail](https://isidore.science/as).

### L'espace personnel pour les chercheurs et chercheures

Isidore.science propose un espace personnel pour les chercheurs et chercheures permettant :

- de collecter, de classer, d’organiser les documents trouvés ;
- d’y regrouper l’ensemble de sa production scientifique afin de l’éditorialiser dans une page de profil personnel ;
- d’y suivre les productions de collègues ;
- d’y enregistrer et d'y publier ses requêtes et leurs résultats à des fins de veille ;
- d’y constituer des bibliographies exportables vers Zotero ;

### Les API d'isidore.science

Les [API du moteur de recherche d'isidore.science](https://api.isidore.science) sont disponibles à l'URL [https://api.isidore.science](https://api.isidore.science) par la méthode GET sur HTTP ou HTTPS.
Elles offrent un service de requêtage des données d'ISIDORE à la fois rapide, précis et fiable avec des fonctionnalités de recherche élaborées (auto-complétion, correction orthographique, recherches multi-critères, booléenne et à facettes, tri, agrégation des réponses, etc).

Chaque requête au moteur est soumise au moyen d'une URI pointant vers un service Web spécifique. La réponse est un flux au format XML (format par défaut) ou JSON.

La page Web sur les [API d'isidore.science](https://api.isidore.science) détaille l'ensemble des commandes disponibles pour les différents services disponibles.

### Les métadonnées enrichies pour le *Linked Open Data*

Les information des métadonnées, ontologies et référentiels d'ISIDORE sont disponibles au sein d'un entrepôt de triplets [RDF (Resource Description Framework) ou *TripleStore*](https://fr.wikipedia.org/wiki/Resource_Description_Framework), plaçant ainsi les données d'ISIDORE dans le *Linked Open Data*. Une interface Web permettant d'interroger à l'aide du language SPARQL et de parcourir le graph d'ISIDORE est disponible via :

- Une interface d'interrogation SPARQL documentée et présentation du modèle de données d'ISIDORE : https://isidore.science/sqe  
- L'interface de base du logiciel Virtuoso : https://isidore.science/sparql

Dans le *TripleStore* d'ISIDORE, les principaux vocabulaires de structuration documentaire des informations sont :

- RDF et RDFS
- Dublin Core Element Set
- Duclin Core TERMS
- SIOC
- FOAF
- OWL
- SKOS
- ORE
- DBPEDIA

(La liste complète est disponible sur <https://isidore.science/sparql?nsdecl>)


### Complémentarité entre ISIDORE et Zotero

#### Utilisation depuis ISIDORE du connecteur Zotero pour alimenter sa base bibliographique

ISIDORE est compatible avec Zotero et permet d’importer les références des documents sur deux niveaux dès lors que l’utilisateur a installé [le connecteur Zotero](https://www.zotero.org/download/) dans son navigateur :

- Sur la page listant les résultats d’une recherche,
- Dans la page de visualisation d’un document.

#### Utilisation depuis Zotero du connecteur de recherche d'ISIDORE

Zotero (client Linux, MacOS, Windows) permet d’utiliser des moteurs de recherche pour rechercher ou compléter des références bibliographiques directement depuis l’interface de Zotero. Nous proposons ici deux connecteurs ISIDORE pour Zotero permettant d’utiliser ISIDORE à partir de recherche sur les auteurs.

L’ajout d’ISIDORE à Zotero permet :

- De compléter des références à partir d’une recherche sur le nom de l’auteur : c’est le "ISIDORE, aide-moi à trouver ce qu’il/elle a publié."
- De trouver des documents dans lequel l’auteure ou l’auteur est cité : c’est le "ISIDORE, qu’as-tu sur l’auteur/auteure ?"

Ces [connecteurs et la documentation d'installation sont disponibles sur le GitLab de la TGIR Huma-Num](https://gitlab.huma-num.fr/spouyllau/ISIDORtero).

### Utilisation des flux RSS

ISIDORE peut proposer ses résultats de recherche sous la forme de flux RSS dans le but d'alimenter des logiciels de veille scientifique (dont Zotero par exemple), des carnets de recherche, etc. Les flux RSS créés dans ISIDORE sont mis à jour, comme l’ensemble des contenus du moteur de recherche, une fois par mois environ lors de la mise à jour générale des contenus d'ISIDORE. Ainsi, il est possible de suivre, depuis Zotero, la mise à jour des documents d’ISIDORE issus des requêtes enregistrées.

Pour cela, il faut demander à ISIDORE --- dans son espace personnel en
mode connecté, le lien vers le flux RSS d’une requête enregistrée en
allant, une fois dans votre espace personnel, dans "Mes requêtes" :

![Mon Image](media/isidore.png)

Pour une requête enregistrée, il faut cliquer sur le pictogramme "Flux
RSS de la requête" disponible à droite ![Mon Image](media/isidore-rss-001.png){: style="width:170px"} et d’en copier le lien avec ![Mon Image](media/isidore-requeteRSS.png){: style="width:120px"}.

Le lien copié est de la forme : `https://isidore.science/feed/lt3913`.

Si votre navigateur est équipé d’un module de lecture des flux RSS, il
sera possible d’utiliser ce lien directement dans votre navigateur.
Dans notre exemple, Nous allons l’utiliser dans Zotero.

Dans Zotero, il faut choisir : Nouveau flux > À partir de l’URI :

![Mon Image](media/zot-001.png){: style="width:60%;margin-left:20%"}

Puis d’ajouter l’url du flux fournit par ISIDORE (avec le navigateur
Safari sous MacOS prendre soin de retirer la mention "feed:" de
l’url). Venir ensuite le coller dans "URL" de la fenêtre de création
de flux RSS de Zotero, exemple ci-dessous :

![Mon Image](media/zot-002.png)

Il faut ensuite donner un titre à son flux, par exemple :
"isidore.science - veille sur ...".

## Que trouve-t-on dans ISIDORE ?

### Organisation des documents et données dans ISIDORE

ISIDORE contient plusieurs millions de documents en SHS qui moissonnés, enrichis avec des référentiels scientifiques et indexés. Ils sont organisés en :

- Documents et données de la recherche (fonds d'archives, matériaux bruts, photographies, films, jeux de données, statistiques, etc) et sont identifiés dans l'ontologie d'ISIDORE par : http://isidore.science/class/primaires
- Documents et données publiées (articles, livres, mémoires et thèses, rapports, etc.) et sont identifiés dans l'ontologie d'ISIDORE par : http://isidore.science/class/secondaires
- Événements scientifiques (colloques, journées d'études, etc.) et sont identifiés dans l'ontologie d'ISIDORE par : http://isidore.science/class/evenementielles


Pour un grand nombre de disciplines des SHS, ISIDORE permet de rechercher des documents venant des principales plateformes de publications du monde entier, ainsi qu’un grand nombre des fonds numérisés par les bibliothèques nationales, universitaires et
municipales.
Pour des usages poussés de recherche, la [recherche avancée d’ISIDORE](https://isidore.science/as) offre par exemple, la
possibilité de rechercher des documents entre deux dates et par discipline ou encore par collections.

Les principales plateformes de publications (revues et livres) présentes dans ISIDORE sont :

- OpenEdition
- Cairn
- Persée
- Erudit
- Oapen
- Redalyc
- Scielo Books

La liste complète des collections contenant des publications peut être obtenu en requêtant [le 3store d'ISIDORE](https://isidore.science/sqe) avec la SPARQL [suivante](https://isidore.science/sparql?query=SELECT+*+WHERE+%7B%0D%0A%3Fs+rdf%3Atype+%3Chttp%3A%2F%2Fisidore.science%2Fclass%2FCollection%3E.%0D%0A%3Fs+rdf%3Atype+%3Chttp%3A%2F%2Fisidore.science%2Fclass%2Fpublications%3E.%0D%0A%3Fs+dcterms%3Atitle+%3Ftitre%0D%0A%7D+ORDER+BY+ASC%28%3Ftitre%29&format=text%2Fhtml&debug=on&timeout=0) :

```
SELECT * WHERE {
 ?s rdf:type <http://isidore.science/class/Collection>.
 ?s rdf:type <http://isidore.science/class/publications>.
 ?s dcterms:title ?titre
} ORDER BY ASC(?titre)
```
Les principales bibliothèques numériques (municipales, nationales, etc.) présentes dans ISIDORE sont :

- Gallica
- Sélène
- E-rara
- NuBIS
- Octaviana
- Burgerbibliothek
- Berkeley Library Digital Collections
- Argonnaute
- BNE
- Cornell
- Didόmena

La liste complète des collections contenant des fonds d'archives et collections de livre peut être obtenu en requêtant [le 3store d'ISIDORE](https://isidore.science/sqe) avec la SPARQL [suivante](https://isidore.science/sparql/?default-graph-uri=&query=SELECT+*+WHERE+%7B%0D%0A%3Fs+rdf%3Atype+%3Chttp%3A%2F%2Fisidore.science%2Fclass%2FCollection%3E.%0D%0A%3Fs+rdf%3Atype+%3Chttp%3A%2F%2Fisidore.science%2Fclass%2Fprimaires%3E.%0D%0A%3Fs+dcterms%3Atitle+%3Ftitre%0D%0A%7D+ORDER+BY+ASC%28%3Ftitre%29&format=text%2Fhtml&timeout=0&debug=on) :

```
SELECT * WHERE {
 ?s rdf:type <http://isidore.science/class/Collection>.
 ?s rdf:type <http://isidore.science/class/primaires>.
 ?s dcterms:title ?titre
} ORDER BY ASC(?titre)
```

### Organisation des documents et données par *types* dans ISIDORE

#### L'ontologie des *types* d'ISIDORE

ISIDORE range également les documents et les données par leurs natures, ou leurs *types* : c'est à dire par articles, jeux de données, photographies, thèses, etc. Cela permet de proposer le filtre "Type de document" dans l'interface des réponses d'[isidore.science](https://isidore.science).

La plupart des bases de données ou entrepôts de données qui alimentent ISIDORE utilisent un ou plusieurs vocabulaires normalisés pour exprimer ces *types*. Le plus souvent, ils sont exprimés à l'aide de métadonnées en *Dublin Core* (*Element Set* ou *Terms*, voir ci-dessous la section sur l'OAI et RDFa) et même s'il existe des référentiels de *types* (voir ci-dessous COAR), nous constatons une très grande hétérogénéité entre les producteurs de données.

Afin de rassembler le plus de documents dans une série exhaustive de *types*, ISIDORE réalise plusieurs traitements sur ces derniers. Il s'agit principalement de regrouper et de ranger les *types* en partant du *type* donné par le producteur de données et en l'alignant sur des URI de référentiels internationaux. Ces traitements de regroupement se font à l'aide d'une "l'ontologie des *types* d'ISIDORE" dont les entrées sont alignées sur les référentiels internationaux COAR, BIBO, RDFS, DCAT, Wikidata.

L'ontologie des *types* ISIDORE est [disponible en ligne](https://isidore.science/sparql?default-graph-uri=&query=PREFIX+skos%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2004%2F02%2Fskos%2Fcore%23%3E%0D%0Aselect+distinct+*+where+%7B%0D%0A%3Chttp%3A%2F%2Fisidore.science%2Fontology%23dataset%3E+%3Fpredicat+%3Fobjet%0D%0A%7D&format=text%2Fhtml&timeout=0&debug=on), via le 3store d'ISIDORE :

```
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
select distinct * where {
?Types_ISIDORE skos:inScheme <http://isidore.science/ontology>
}
```

Dans la mesure où il s'agit d'un traitement effectué par ISIDORE, il est à noter que les labels de l'ontologie des types d'ISIDORE sont disponibles en anglais, français et espagnol comme le reste des enrichissements créés par ISIDORE.

#### Alignement des *types* entre ISIDORE et NAKALA

Dans l'écosystème d'Huma-Num, depuis la refonte de NAKALA en 2020, le référentiel des *types* est [le référentiel international "COAR" (Confederation of Open Access Repositories)](https://www.coar-repositories.org) développé dès 2008 dans le cadre du programme européen DRIVER et largement utilisé à l'échelle internationale dans la plupart des plateforme de données de science (OpenAIRE, etc.).

En 2020, un alignement des types de l'ontologie d'ISIDORE et ceux de NAKALA, à l'aide de COAR, a été mise en œuvre. Ce aligement sert à la fois dans l'interface de dépôt des données de NAKALA (elle alimente la liste déroulante "Type de dépôt") et dans ISIDORE dans l’ontologie des types. Cet aligement peut être obtenu en requêtant à l'aide des 3stores RDF d’ISIDORE ou de NAKALA.

Pour ISIDORE, avec [la SPARQL suivante](https://isidore.science/sparql?default-graph-uri=&query=SELECT+*+WHERE+%7B%0D%0A%3FTypeISIDORE+skos%3AinScheme+%3Chttp%3A%2F%2Fisidore.science%2Fontology%3E.%0D%0A%3FTypeISIDORE+skos%3AprefLabel+%3FLabelISIDORE.%0D%0A%3FTypeISIDORE+owl%3AequivalentClass+%3FTypeCOARpourNAKALA%0D%0A%7D&format=text%2Fhtml&timeout=0&debug=on) :

```
SELECT * WHERE {
?TypeISIDORE skos:inScheme <http://isidore.science/ontology>.
?TypeISIDORE skos:prefLabel ?LabelISIDORE.
?TypeISIDORE owl:equivalentClass ?TypeCOARpourNAKALA
}
```
!!! attention "À noter"
    Une présentation croisée des 3stores sera proposée dès que NAKALA proposera un 3store sur sa nouvelle version. Il est pour le moment possible d'utiliser les API de NAKALA et d'ISIDORE pour cela.


### Indexation des principales plateformes de données en SHS

ISIDORE moissonne, c'est le terme consacré, puis indexe les contenus de nombreuses plateformes de données en SHS permettant aux chercheurs de regrouper dans leur profil d'utilisateur l'ensemble de leurs données. Nous encourageons les chercheurs et chercheures, pour leurs programmes de recherche, à utiliser des plateformes proposant des dispositifs et protocoles d'interopérabilité ouverts permettant de présenter des métadonnées documentaires et scientifiques.

Les principales plateformes de données (sources, archives mais aussi publications) sont moissonné par ISIDORE.

La liste complète des collections peut être obtenu en requêtant [le 3store d'ISIDORE](https://isidore.science/sqe) avec la SPARQL [suivante](https://isidore.science/sparql/?default-graph-uri=&query=SELECT+*+WHERE+%7B%0D%0A+%3Fs+rdf%3Atype+%3Chttp%3A%2F%2Fisidore.science%2Fclass%2FCollection%3E.%0D%0A+%3Fs+dcterms%3Atitle+%3Ftitre%0D%0A%7D+ORDER+BY+ASC%28%3Ftitre%29%0D%0A&format=text%2Fhtml&timeout=0&debug=on) :

```
SELECT * WHERE {
 ?s rdf:type <http://isidore.science/class/Collection>.
 ?s dcterms:title ?titre
} ORDER BY ASC(?titre)
```

N'hésitez pas à nous en signaler.

#### Les données déposées et documentées dans NAKALA peuvent-elles être référencées par ISIDORE ?

Oui, les données déposés et documentées dans NAKALA peuvent être
accessibles dans ISIDORE. NAKALA propose en standard le protocole d'interopérabilité [OAI-PMH](https://fr.wikipedia.org/wiki/Open_Archives_Initiative_Protocol_for_Metadata_Harvesting) qui permet de moissonner, c'est le terme consacré, les métadonnées des documents, et donc
de les référencer, enrichir et indexer par ISIDORE.

Le référencement par moissonnage OAI-PMH n’est cependant pas
automatique pour le moment, notamment pour permettre aux utilisateurs de préparer et d'organiser leurs
données et métadonnées. Pour être référencé, il suffit de demander par e-mail à être indexé ISIDORE via <isidore-sources@huma-num.fr>.

#### Comment des articles et images scientifiques déposées dans l’archive ouverte HAL, HAL-SHS et MédiHAL seront-elles accessibles dans ISIDORE ?

Tous les fichiers (PDF, illustrations, photographies, audio et vidéo) déposés et documentés dans l’archive ouverte HAL, dont HAL-SHS, ainsi que MédiHAL sont automatiquement référencés dans ISIDORE et indexés au niveau de leurs métadonnées. Tous ces documents et leurs notices sont donc accessibles à travers les différentes interfaces d’interrogation d’ISIDORE.

#### Les données déposées dans l'entrepôt Didómena (EHESS) peuvent-elles être référencées par ISIDORE ?

Oui, [Didómena](https://didomena.ehess.fr) (l'entrepôt de données de la recherche de l'EHESS) propose une interopérabilité en OAI-PMH. Attention, le moissonnage n'est pas automatique. Pour être référencé au niveau de votre collection, merci de nous communiquer le point d'accès OAI-PMH via <isidore-sources@huma-num.fr>.

#### Les données déposées dans Calames (ABES) peuvent-elles être référencées par ISIDORE ?

Oui, les descriptions de fonds d'archives catalogués dans [Calames](http://calames.abes.fr) (le catalogue des archives et des manuscrits des bibliothèques universitaires françaises) sont indexés dans ISIDORE. Cependant, la norme EAD-XML, utilisé dans Calames, ne permet pas toujours une indexation documentaire optimale : principalement au niveau de la richesse des métadonnées. Ceci est dû à la logique propre à la norme EAD-XML dans l'encodage des informations dans les niveaux de description des fonds.

#### Les données déposées dans l'entrepôt Data.sciencespo peuvent-elles être référencées par ISIDORE ?

Oui, les données déposées et documentées dans [Data.sciencespo](https://data.sciencespo.fr) (Dataverse) propose une interopérabilité en OAI-PMH. Il est moissonné automatiquement par ISIDORE.

#### Les données déposées dans la plateforme COCOON peuvent-elles être référencées par ISIDORE ?

Oui, les données déposées et documentées dans [la plateforme COCOON](https://cocoon.huma-num.fr) propose une interopérabilité en OAI-PMH. Cette plateforme est moissonnée automatiquement par ISIDORE.

#### Les fichiers et documents déposés dans la plateforme européenne Zenodo peuvent-ils être référencés par ISIDORE ?

Oui, il est possible pour ISIDORE de référencer les fichiers et
documents déposés et documentés sur la plateforme
[Zenodo](https://zenodo.org).

Le référencement repose sur le principe du moissonnage OAI-PMH sur un
ensemble de fichiers et données (et donc leurs métadonnées) correspondant à un ou
des identifiant(s) correspondants aux identifiants des "communities" dans Zenodo (voir https://developers.zenodo.org/#sets).
Nous pouvons aussi regrouper plusieurs identifiants Zenodo dans une même
collection ISIDORE permettant ainsi aux déposants de plusieurs corpus
déposés dans Zenodo de les regrouper dans ISIDORE pour leur donner plus
de visibilité.

Pour ajouter dans ISIDORE vos dépôts Zenodo, [merci de nous envoyer
l’URL
OAI-PMH](mailto:isidore-sources@huma-num.fr?subject=%22Je%20souhaiterai%20faire%20moissonner%20mes%20dépôts%20Zenodo%22)
de votre dépôt (voir <https://developers.zenodo.org/#oai-pmh>).

#### Les bibliothèques numériques utilisant « Gallica marque blanche » peuvent-elles être référencées par ISIDORE en tant que collection d'ISIDORE ?

Oui, car la BnF expose selon le protocole OAI-PMH les contenus de la bibliothèque numérique utilisant [« Gallica marque blanche »](https://www.bnf.fr/fr/gallica-marque-blanche) sous la forme d'un [« Set » OAI-PMH](https://documentation.huma-num.fr/isidore/#comment-signaler-ses-donnees-dans-isidore-avec-des-metadonnees-et-le-protocole-oai-pmh). Pour être référencé au niveau de votre collection, merci de nous communiquer le point d'accès OAI-PMH via <isidore-sources@huma-num.fr>.


#### ISIDORE peut-il moisonner les sites Omeka mis en œuvre par l'INIST-CNRS ?

Oui, les sites réalisés par l'INIST-CNRS dans le cadre de leur [offre de service  de sites utilisant Omeka](https://www.inist.fr/realisations/omeka-pour-des-bases-de-donnees-valorisees/) peuvent être signalés dans ISIDORE en tant que collection. Les métadonnées sont moissonnables et les données, y compris du texte intégral (PDF, XML, etc.) peuvent donc être indexées et enrichies par ISIDORE. L'INIST propose une page dédié à son offre : https://www.inist.fr/realisations/omeka-pour-des-bases-de-donnees-valorisees/.

## Comment faire pour que des données soient référencées par ISIDORE ?

Il y a plusieurs façons de faire référencer des données et documents par
ISIDORE :

-   Proposer ses données via [un flux XML de métadonnées normalisées et
    utilisant le protocole OAI-PMH](#comment-signaler-ses-donnees-dans-isidore-avec-des-metadonnees-et-le-protocole-oai-pmh) associé à des métadonnées au format
    Dublin core. Cette méthode est adaptée pour les bases de données
    documentaires, les corpus, les fonds d’archives scientifiques et les
    bibliothèques de documents/données. A titre d’exemple, [un outil tel
    que Omeka (Classic ou S) propose le protocole OAI-PMH via des modules](#un-site-Web-utilisant-omeka-classic-et-omeka-s-peuvent-il-etre-reference-par-isidore).
    Cette méthode est adaptée aux sites Web de programme de recherche présentant des corpus de documents ou de données, blogs scientifique (hors Hypotheses.org), et pages Web en général.

Ces deux méthodes sont par ailleurs souvent implémentées par des outils de publication de données (CMS, SIGB, etc.), par exemple :



### Comment signaler ses données dans ISIDORE avec des métadonnées et le protocole OAI-PMH ?

Pour signaler ses données dans ISIDORE en utilisant le protocole
OAI-PMH, il suffit :

-   De préparer ses données et ses métadonnées en utilisant le
    vocabulaire documentaire Dublin Core Element Set ou le Dublin Core
    Terms, suivant le niveau de précision que l’on souhaite et de les
    rendre accessibles via [le protocole OAI-PMH](https://fr.wikipedia.org/wiki/Open_Archives_Initiative_Protocol_for_Metadata_Harvesting) ;
-   D’organiser et de documenter les *Sets* de son entrepôt OAI-PMH
-   De signaler à <isidore-sources@huma-num.fr> l’adresse de son
    entrepôt à Huma-Num.

#### Les ensembles de document en OAI-PMH : les *Sets*

Le protocole OAI-PMH permet, par la création de *Sets*, de rassembler en un
ensemble cohérent des notices dont le périmètre fait sens sur le plan scientifique ou éditorial et qui est laissé à la libre appréciation du producteur des données.

Il permet aussi de définir une hiérarchie dans les *Sets* avec un mécanisme d’héritage en précisant
dans le nom du set le nom du ou des *Sets* parents et du *Set* enfant
séparé par le caractère `:`. ISIDORE est en capacité d’utiliser ces
*Sets* pour limiter le moissonnage à un ensemble de notices ou pour
différencier différentes sources de données au sein d’un même entrepôt.
Le producteur devra donc préciser les modalités de moissonnage qui lui
paraissent les plus appropriées afin de valoriser au mieux ses
ressources au sein d’ISIDORE. Pour cela, il indiquera le ou les *Sets*
concernés ou une règle permettant de distinguer les *Sets* à prendre en
compte.

Les *Sets* peuvent présenter des métadonnées, en Dublin Core Element Set, qui leurs sont propres. Par exemple :

```xml
<set>
 <setSpec>OuvColl</setSpec>
 <setName>Ouvrages</setName>
 <setDescription>
  <oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
   <dc:description>Ouvrages de recherche diffusés sur Cairn.info</dc:description>
  </oai_dc:dc>
 </setDescription>
</set>
```

#### Les notices en OAI-PMH ou *Records* :

Dans le cadre d’ISIDORE, chaque *record* OAI-PMH correspond à un document.
Le moissonneur d’ISIDORE exploite ainsi les métadonnées décrites selon le
profil d’applications défini par l’Open Archive Initiative pour le
_Dublin Core Element Set_ (connu aussi Dublin Core "simple"). De
surcroît, le moissonneur collecte également le ou les documents en texte
intégral dont les URL (débutant par `https://` ou `http://`) sont indiquées
dans l’élément `<dc:identifier>`.

Nous recommandons aux producteurs de données de proposer des records les
plus riches possible en métadonnées. En effet, la pertinence dans
ISIDORE favorise les métadonnées les plus riches possibles. Des champs
tel que :

```xml
<dcterms:description>
<dcterms:creator>
<dcterms:date>
```

sont indispensables.

##### Exemple d’une notice complète selon le protocole OAI-PMH :

```xml
<record>
 <header>
  <identifier>oai:halshs.archives-ouvertes.fr:halshs-00514304</identifier>
  <datestamp>2010-09-02T11:06:50Z</datestamp>
  <setSpec>halshs</setSpec>
  <setSpec>SHS:ECO</setSpec>
  <setSpec>SDV:BIO</setSpec>
  <setSpec>INFO:INFO_BT</setSpec>
  <setSpec>SDV:SA:AEP</setSpec>
  <setSpec>SDV:SA:STA</setSpec>
  <setSpec>CIRAD</setSpec>
  <setSpec>SHS</setSpec>
 </header>
 <metadata>
  <oai_dc:dc xsi:schemaLocation=”http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd”>
  <dc:identifier>http://halshs.archives-ouvertes.fr/halshs-00514304/en/ </dc:identifier>
  <dc:identifier>http://halshs.archives-ouvertes.fr/docs/00/51/43/98/PDF/Regulation_GMO_pprint.pdf</dc:identifier>
  <dc:identifier>http://halshs.archives-ouvertes.fr/docs/00/51/43/98/PDF/ppt_nocmt_broader_regulation.pdf </dc:identifier>
  <dc:title>Broadening the scope of regulation: a prerequisite for a positive contribution of transgenic crop useto sustainable development</dc:title>
  <dc:creator>Fok, Michel</dc:creator>
  <dc:subject>[SHS:ECO] Humanities and Social Sciences/Economy and finances </dc:subject>
  <dc:subject>[SDV:BIO] Life Sciences/Biotechnology</dc:subject>
  <dc:subject>[INFO:INFO_BT] Computer Science/Biotechnology</dc:subject>
  <dc:subject>[SDV:SA:AEP] Life Sciences/Agricultural sciences/Agriculture, economy and politics </dc:subject>
  <dc:subject>[SDV:SA:STA] Life Sciences/Agricultural sciences/Sciences and technics of agriculture</dc:subject>
  <dc:subject>regulation</dc:subject>
  <dc:subject>coordination</dc:subject>
  <dc:subject>GMO</dc:subject>
  <dc:subject>biotechnology</dc:subject>
  <dc:subject>seed price</dc:subject>
  <dc:subject>research</dc:subject>
  <dc:subject>weed resistance</dc:subject>
  <dc:subject>pest complex shift</dc:subject>
  <dc:description>Ex-ante regulation of transgenic crop use generally prevails, before the authorization of commercial release.This kind of regulation addresses the concerns of biosafety and coexistence, under pressure of pros and/or cons of GMO. After fifteen years of large scale use of transgenic crops (notablysoybean and cotton) in various countries (USA, China, Brasil, India...), ecological and economic phenomena are observed and which could threaten the sustainable use of transgenic varieties. I advocate that the regulation scope must be extended so as to a) promote a systemic and coordinatedapproach of transgenic crop use, b) ensure seed purity with regard to the transgenic trait, c) maintain research on non-transgenic varieties, and d) warrant fair pricing of transgenic seeds.</dc:description>
  <dc:coverage>Montpellier</dc:coverage>
  <dc:coverage>France</dc:coverage>
  <dc:date>2010-08-29</dc:date>
  <dc:language>English</dc:language>
  <dc:type>proceeding with peer review</dc:type>
  <dc:source>Proceedings of Agro2010, the XIth ESA Congress</dc:source>
  <dc:source>Agro2010, the XIth ESA Congress</dc:source>
 </oai_dc:dc>
</metadata>
</record>
```

En plus de cette description en *Dublin Core Element Set*, chaque
enregistrement peut être décrit suivant un ou plusieurs formats de
métadonnées dont le choix est laissé à l’appréciation de
l’administrateur de l’entrepôt OAI-PMH.

Le moissonneur d’ISIDORE est en capacité d’exploiter le format *Dublin Core Terms* et tous schémas XML permettant
l’exposition du texte intégral (dont la TEI ou l’EAD) améliorant ainsi
son indexation. Le producteur de données devra veiller à respecter
scrupuleusement les spécifications du protocole OAI-PMH dans sa version
2.0 en particulier sur :

- Le respect strict des valeurs de "datestamp" dans les *records* afin de synchroniser au mieux les mise à jour entre le producteur et ISIDORE ;
- La bonne gestion des données supprimées ([détail sur la documentation du protocole OAI-PMH](http://www.openarchives.org/OAI/openarchivesprotocol.html#DeletedRecords)) ;
- Dans le cadre d'entrepôt de données d'éditeurs ou de taille importante, l'accès à son entrepôt OAI-PMH par les adresses IPs des moissonneurs OAI-PMH d’ISIDORE (signalement du moissonnage par ISIDORE auprès de sa DSI).

Nous conseillons aux producteurs de valider régulièrement la conformité
de leur entrepôt grâce, par exemple, aux [outils de l’Open archive
initiative](https://www.openarchives.org/pmh/tools/). Enfin, nous conseillons aux producteurs de données de contacter l'équipe d'Huma-Num pour toutes demandes d'informations.

### Comment signaler ses données dans ISIDORE avec des métadonnées RDFa ?

Le RDFa permet d'exprimer une structure de métadonnées selon les principes du Web sémantique (RDF pour *[Resource Description Framework](https://fr.wikipedia.org/wiki/Resource_Description_Framework)*) dans le code HTML de pages Web. Le "a" de RDFa veut dire "in
attributes", c'est à dire au sein du code HTML).

Comment exprimer des métadonnées d’une page Web très simplement en
utilisant la [syntaxe
RDFa](https://tcuvelier.developpez.com/tutoriels/Web-semantique/rdfa/introduction/)
? Par exemple, dans un billet de blog publié avec WordPress. S’il
peut exister des [plugins pour faire
cela](https://wordpress.org/plugins/search/RDFa/),
l’obsolescence de ces derniers peut rendre difficile leur maintien dans
le temps. Une autre solution consiste à implémenter RDFa dans le code
HTML du thème WordPress que l’on a choisi. Pour ce que cela soit facile
et gérable dans le temps, le plus simple est d’utiliser l’entête HTML
`<head>` afin d’y placer des balises `<meta>` qui contiendront quelques métadonnées.

Exprimer des métadonnées selon le modèle RDF via la syntaxe RDFa permet
à des machines (principalement des moteurs de recherche et des indexeurs) de mieux traiter l’information car elle devient plus explicite : pour une machine, une chaîne de caractère peut être un titre ou un résumé, si vous ne lui dites pas que c’est un titre ou que c’est un résumé elle
ne le devinera pas. A minima, il est donc possible d’utiliser les
balises `<meta>` pour définir une structure RDF offrant la possibilité
de structurer les métadonnées minimales par exemple avec le vocabulaire
documentaire Dublin Core Element Set.

#### Comment faire pratiquement ?

En premier, il faut indiquer dans le DOCTYPE de la page Web, qu’elle va
contenir des informations qui vont utiliser le modèle RDF, ainsi, le
DOCTYPE sera :

```xml
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
```

Dans la balise `<html>`, doivent être présente les adresses des
ontologie documentaires (via leurs *NameSpace XML*) qui servent
à "typer" les informations. RDFa — qui place des métadonnées dans le Web sémantique, nécessite à minima de faire appel aux ontologies RDF et RDF Schema et au Dublin Core Element Set (dc). Il est possible d'utiliser en plus — afin d'affiner les métadonnées, le Dublin Core Terms (dcterms) :

```xml
<html xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:dcterms="http://purl.org/dc/terms/">
```

Il est possible, pour encoder plus d’information, d’utiliser plus
d'ontologies documentaires :

```xml
<html
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:skos="http://www.w3.org/2004/02/skos/core#"
xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
xmlns:foaf="http://xmlns.com/foaf/0.1/"
xmlns:cc="http://creativecommons.org/ns#">
```

Dans l'exemple ci-dessus, [foaf](http://www.foaf-project.org/) sert à encoder des informations relatives à une personne ou un objet décrit par les métadonnées. L'ontologie [CC](https://creativecommons.org) permet de signaler quelle licence, issues des *Creative Commons*, s’appliquerait à ce contenu.

La structure RDFa au travers de balises
`<meta>` dans l’en-tête `<head>` de la page HTML. Dans un premier
temps, à l’aide d’une balise `<link>`, nous allons définir l’objet
numérique auquel les informations encodées en RDF seront rattachées :

```xml
<link rel="dc:identifier" href="http://monblog.com/monbillet.html" />
```

Cette balise définit donc un conteneur pour les informations que nous
allons indiquer à l’aide des balises `<meta>`. Ce conteneur est
identifié par une URI qui se trouve être une URL, c’est à dire
l’adresse de la page dans le Web.


Les balises `<meta>` définissent ensuite un ensemble de métadonnées, c’est à dire dans notre cas, des informations descriptives de la page Web du billet du blog :

```xml
<meta property="dc:title" content="Le titre de mon billet" />
<meta property="dc:creator" content="Prénom Nom de l'auteur 1" />
<meta property="dc:creator" content="Prénom Nom de l'auteur 2" />
<meta property="dcterms:created" content="2011-01-27" />
<meta property="dcterms:abstract" content="Un résumé descriptif du contenu de ma page" xml:lang="fr" />
<meta property="dcterms:abstract" content="A summary in english" xml:lang="en" />
<meta property="dc:subject" content="mot-clé 3" />
<meta property="dc:subject" content="mot-clé 2" />
<meta property="dc:type" content="billet" />
<meta property="dc:format" content="text/html" />
<meta property="dc:relation" content="Un lien vers une page Web complémentaire" />
```

Suivant la nature du contenu de la page Web, il est bien sûr possible
d’être plus précis, plus fin et plus complet dans les informations
encodées. Par exemple, il sera judicieux d’utiliser le vocabulaire DC
Terms.

Le DC Terms permet par exemple d'inclure une forme précise pour une référence bibliographique du contenu :


```xml
<meta property="dcterms:bibliographicCitation" content="Mettre ici une référence bibliographique" />
```

Il serait possible de passer l’ensemble du texte d’une page Web à l’aide du vocabulaire SIOC [en utilisant la propriété
`sioc:content`](http://www.lespetitescases.net/rdfaiser-votre-blog-2-la-pratique).

Il est possible également de relier des pages Web entre elles (pour
définir un corpus d’auteurs par exemple) en utilisant dans le
vocabulaire DC Terms la propriété du DC Terms : `dcterms:isPartOf`.

```xml
<meta property="dcterms:isPartOf" content="URL d'une autre page Web" />
```

#### Création du Sitemap

Une fois l’encodage RDFa fait dans les pages HTML, il vous reste à créer
un fichier XML de type Sitemap listant les pages que vous souhaitez qu’ISIDORE moissonne et soumettre l’URL de ce sitemap :

```xml
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc>http://monsiteWeb.com/</loc>
		<lastmod>2018-01-01</lastmod>
		<changefreq>monthly</changefreq>
		<priority>1.0</priority>
	</url>
	<url>
		<loc>http://monsiteWeb.com/page1/</loc>
		<lastmod>2018-03-05</lastmod>
		<changefreq>weekly</changefreq>
		<priority>0.5</priority>
  </url>
</urlset>
```

Il est possible de tester l’extraction que fera ISIDORE de vos
métadonnées RDFa à l’aide de l’application "ISIDORE à la demande"
disponible sur à l'adresse <https://rd.isidore.science/ondemand/fr/rdfa.html>

### Moissonnage par OAI pour les *Content Management System* (CMS)

#### Un site Web utilisant Omeka Classic et Omeka-S peuvent-il être référencé par ISIDORE ?

Oui, Omeka *Classic* et Omeka S proposent des modules permettant d'exposer les métadonnées selon le protocole OAI-PMH :

- Module pour [Omeka S](https://omeka.org/s/modules/OaiPmhRepository/)
- Module pour [Omeka Classic](https://omeka.org/classic/docs/Plugins/OaiPmhRepository/)

#### Un site Web utilisant WordPress peut-il être référencé par ISIDORE ?

Oui, il existe [plusieurs modules WordPress](https://fr.wordpress.org/plugins/search/oai/) pour exposer en OAI-PMH des contenus sous WordPress. Cependant, une implémentation en [Sitemap+RDFa](https://documentation.huma-num.fr/isidore/#comment-signaler-ses-donnees-dans-isidore-avec-des-metadonnees-rdfa) permet de travailler à une exposition plus fine sur les plans scientifiques et documentaires des contenus et des métadonnées et par ailleurs ne pas être dépendant de l'obsolescence des modules WordPress.

#### Un site Web utilisant Drupal peut-il être référencé par ISIDORE ?

Oui, il est possible de faire indexer par ISIDORE des pages Web générées
par le CMS Drupal. Il y a deux façons de faire, suivant la nature des
contenus de vos pages :

-   Soit via le protocole OAI-PMH et dans ce cas il existe plusieurs
    modules pour Drupal, voir sur
    [https://www.drupal.org/search/site/OAI-PMH](https://www.drupal.org/search/site/OAI-PMH?f%5B0%5D=ss_meta_type%3Amodule "OAI-PMH pour Drupal").
-   Soit via l’utilisation d’une structure de métadonnées en Dublin
    Core dans les pages Web générées par Drupal utilisant RDFa et un
    sitemap.xml. Un article dédié à cette façon de procéder est
    disponible à l’adresse ci-dessus.




## Périmètre d'ISIDORE

### Pourquoi certains articles ne se retrouvent pas dans ISIDORE ?

Si vous ne retrouvez pas la totalité de votre production scientifique
dans [ISIDORE](https://isidore.science), il peut y avoir plusieurs
explications. Il se peut que vos articles soient publiés dans des revues
qui ne sont pas électroniques ou qui ne rendent pas accessibles leurs
articles même longtemps après leur publication. En effet, depuis sa
création, [ISIDORE](https://isidore.science) favorise l’open
access : l’indexation est meilleure pour les articles disponibles en
accès libre. De nombreuses revues électroniques ont fait ce choix au
travers de portails tels que Open Edition Journal (anciennement
Revues.org), Érudit, Persée, et Cairn.info, Redalyc, OApen et les articles de
ces revues sont donc collectés et indexés par
[ISIDORE](https://isidore.science).

Il se peut également que vos articles soient publiés en ligne, mais pas
sur une plateforme d’édition électronique (mais un site Web), ou sur une
plateforme d’édition électronique ne permettant pas l’indexation via le
protocole standard (voir la question-réponse sur l’OAI-PMH).

D’autres revues rendent accessibles leurs articles, mais seulement après
une période d’embargo. Dans ce cas,
[ISIDORE](https://isidore.science) n’indexe que les métadonnées
de l’article. Si vous vous connectez via votre bibliothèque
universitaire, centre de documentation ou par BibCNRS, il est possible
que vous ayez quand même accès à ces articles.

Il est possible de rechercher dans les collections indexées par
[ISIDORE](https://isidore.science) en utilisant le moteur lui-même et en
indiquant que vous souhaitez recherche dans les collections.

Il se peut aussi que votre article soit publié sous forme de PDF image,
dans ce cas seul le référencement par
[ISIDORE](https://isidore.science) sera permis, mais pas son
indexation en texte intégral.

Il se peut enfin que certains de vos articles soient publiés dans des
revues qui ne sont pas classées en SHS.

Dans tous ces cas, vous pouvez vous-même déposer vos articles dans une
archive ouverte comme HAL (HAL-SHS en particulier) qui est aussi indexée par
[ISIDORE](https://isidore.science) ou vous rapprocher de votre
bu/centre de documentation.

Si vous n’êtes dans aucun de ces cas et pensez donc qu’il s’agit d’une
erreur, vous pouvez nous envoyer un mail à isidore@huma-num.fr.

### Pourquoi certains ouvrages/chapitres d’ouvrage ne sont pas signalés dans ISIDORE ?

ISIDORE sait identifier qu’un document est de type "ouvrage", ainsi, il y
a plus de 500000 ouvrages et chapitres d’ouvrages signalés dans
ISIDORE.

Il faut savoir qu’il existe relativement peu de plateformes d’édition
d’ouvrages en ligne en libre accès. ISIDORE indexe en SHS, par exemple, les
contenus des plateformes d'ouvrage comme :

- [OpenEdition Books](https://isidore.science/search/?collection=10670/3.szxq6s) (au niveau des chapitres, et de les signaler) ;
- [Scielo Books](https://isidore.science/search/?collection=10670/3.7oraz1) (Brésil) ;
- [OApen](https://isidore.science/search/?collection=10670/3.pwofj8) (Pays-Bas) ;
- [Erudit](https://isidore.science/s/collection?q=erudit) (Canada) ;
- …

Par ailleurs, vous pouvez, en accord avec votre éditeur, déposer votre
ouvrage ou chapitres d’ouvrages dans l’archive ouverte
[HAL-SHS](https://halshs.archives-ouvertes.fr). Il sera alors indexé par
ISIDORE dans le cadre de l’indexation de HAL-SHS et reconnu comme un chapitre d'ouvrage.

### Pourquoi certaines bases de données ne sont pas signalées dans ISIDORE ?

Le moissonnage par ISIDORE nécessite une exposition de métadonnées (documentaires, scientifiques, etc.) standardisées et normalisées (soit en utilisant le protocole OAI-PMH, soit à l'aide d'un Sitemap XML et de métadonnées RDFa, voir ci-dessus).

Si vous connaissez des bases de données qui ne sont pas présentes dans ISIDORE, n'hésitez pas à nous les signaler afin que nous puissions voir avec leurs éditeurs/producteurs de données.

## Formations à ISIDORE

Nous listons ici les formations, présentations fonctionnelles et auto-formations en ligne à l'utilisation d'ISIDORE. N'hésitez pas à nous faire par de formations que vous organiseriez :

- [L'Urfist Méditerranée vous propose une nouvelle formation e-learning sur Isidore](https://urfist.univ-cotedazur.fr/nouvelle-formation-en-ligne-une-initiation-a-isidore/) (mars 2021)
- [« Isidore, mon assistant de recherche personnel »](https://ig.hypotheses.org/2215) par Johanna Daniel (avril 2020)
