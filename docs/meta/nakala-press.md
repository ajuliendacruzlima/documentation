---
lang: fr
---

!!! Note  
    Document en cours de rédaction

### NAKALA Press
Nakala embarque un module de publication dénommé NAKALA_Press. Il s'accède par l'onglet **Sites web** et permet de gérer plusieurs sites NAKALA_Press.
